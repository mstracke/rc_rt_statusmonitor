﻿using RT_RC_StatusWatch.RCInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RT_RC_StatusWatch.Dashboard
{
    public class DatabaseManager
    {

        private static DatabaseManager _this = null;
        const string TOKEN = "14a26373e4e810e4e7d1ccab51fcb761";
        private static readonly HttpClient client = new HttpClient();

        private Dictionary<int, User> StoredUserStore = new Dictionary<int, User>();
        public static readonly object dblock = new object();
        public static DatabaseManager Instance
        {
            get
            {
                lock (dblock)
                {
                    if (_this == null)
                        _this = new DatabaseManager();

                    return _this;
                }


            }
        }

        public DatabaseManager()
        {

        }
        const string baseurl = "https://myrtpos.net/dashboard/bin/";
        const string createUrl = "create_user.php";
        const string updateStatusUrl = "update_status.php";
        const string updatedashStatusUrl = "update_dash_status.php";

        public async void UpdateStatus(User user)
        {
            if (!user.BelongsToMonitoredQueue)
                return;

            var url = baseurl + updateStatusUrl;
            var data = new Dictionary<string, string>()
                {
                    { "token", TOKEN },
                    {"uid", user.UID },
                    {"phone_status", user.UserTelephonyStatus.GetStatusString() },
                    {"dnd_status", user.UserDNDStatus.GetStatusString() },
                    {"presence_status", user.UserPresenceStatus.GetStatusString() },
                    {"combined_status", user.UserCombinedStatus() },
                    {"current_call", user.CurrentCall() },
                    {"last_call", user.LastCall() },                    
                    {"last_event", user.LastEventString },
                    {"todays_hours", user.TodaysHours },
                    {"username", user.UserName }
                };
            var content = new FormUrlEncodedContent(data);
            
            try
            {
                var rep = await client.PostAsync(url, content);
                var res = rep.Content.ReadAsStringAsync();
                Program.LastResults.Add(res.Result);
            }
            catch(Exception e)
            {
                Program.Print(e.Message);
                return;
            }
            
            
            
        }
        /// <summary>
        /// Used to add users to database for 1st time.
        /// </summary>
        /// <param name="user"></param>
        public async void InsertNewUser(User user)
        {
            

            if (user.BelongsToMonitoredQueue)
            {
                Program.Print(string.Format("Adding new User {0}-{1}", user.Extention, user.BelongsToMonitoredQueue));


                var url = baseurl + createUrl;
                var data = new Dictionary<string, string>()
                {
                    { "token", TOKEN },
                    {"name", user.Name },
                    {"ext", user.Extention.ToString() },
                    {"uid", user.UID },
                    {"phone_status", user.UserTelephonyStatus.GetStatusString() },
                    {"dnd_status", user.UserDNDStatus.GetStatusString() },
                    {"presence_status", user.UserPresenceStatus.GetStatusString() },
                    {"combined_status", user.UserPresenceStatus.GetStatusString() },
                    {"dashboard_status", "0" },
                    {"current_call", user.CurrentCall() },
                    {"last_call", user.LastCall() },
                    {"username", user.UserName },
                    {"todays_hours", user.TodaysHours },
                    {"last_event", user.LastEventString },
                };

                var content = new FormUrlEncodedContent(data);
                var rep = await client.PostAsync(url, content);
                var res = rep.Content.ReadAsStringAsync();
                Program.LastResults.Add(res.Result);

            }
        }
    }
}
