﻿using RT_RC_StatusWatch.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RT_RC_StatusWatch
{
    static class Program
    {
        public static Settings settings;
        public static volatile bool IsAlive = true;

        public static volatile bool IsConnected = false;
        public static volatile bool IsSubscribing = false;
        private static List<Thread> ThreadList = new List<Thread>();
        private static List<IDisposable> DisposeList = new List<IDisposable>();

        public static volatile List<string> LastResults = new List<string>();

        [STAThread]
        static void Main()
        {

            var procs = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);
            if(procs.Length == 1)
            {
                settings = new Settings();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmControlPanel());
            }
            else
            {
                MessageBox.Show(string.Format("{0} is Already Running", Process.GetCurrentProcess().ProcessName));
            }

           
        }

        public static void ToDispose(IDisposable item)
        {
            DisposeList.Add(item);
        }

        public static void ToDispose(Thread item)
        {
            ThreadList.Add(item);
        }

        public static void Shutdown()
        {
            IsAlive = false;
            foreach (var t in ThreadList)
                t.Abort();
            foreach (var d in DisposeList)
                d.Dispose();
        }


        public static void SetBackground(Control control, Color color)
        {
            if (control.InvokeRequired)
            {
                control.BeginInvoke((MethodInvoker)delegate ()
                {
                    control.BackColor = color;
                    control.Invalidate();
                });
            }
            else
            {
                control.BackColor = color;
                control.Invalidate();
            }
        }

        internal static void Print(string v)
        {
#if DEBUG
            Console.WriteLine(v);
#endif
            PrintToUI(v);

        }

        private static List<string> OutputList = new List<string>();
        public static readonly object opLock = new object();
        private static void PrintToUI(string v)
        {
            lock (opLock)
            {
                if (OutputList.Count == 0)
                    OutputList.Add(v);
                else
                    OutputList.Insert(0, v);
            }
        }

        public static bool GetUIText(int lineCount, out string[] ret)
        {
            lock (opLock)
            {
                if (lineCount != OutputList.Count)
                {
                    ret = OutputList.ToArray();
                    return true;
                }
                ret = new string[0];
                return false;
            }
        }

        internal static void SetVisible(Control control, bool v)
        {
            if (control.InvokeRequired)
            {
                control.BeginInvoke((MethodInvoker)delegate ()
                {
                    control.Visible = v;
                    control.Invalidate();
                });
            }
            else
            {
                control.Visible = v;
                control.Invalidate();
            }
        }

        public static void SetText(Control control, string t)
        {
            if (control.InvokeRequired)
            {
                control.BeginInvoke((MethodInvoker)delegate ()
                {
                    control.Text = t;
                    control.Invalidate();
                });
            }
            else
            {
                control.Text = t;
                control.Invalidate();
            }
        }
    }
}
