﻿using RingCentral;


namespace RT_RC_StatusWatch.RCInterop
{
    public class Call
    {
        public string Action = string.Empty;
        public string Direction = string.Empty;
        public long? Duration = 0;
        public CallerInfo From;
        public CallerInfo To;
        public string UID = string.Empty;
        public LegInfo[] Legs;
        public string Result = string.Empty;
        public string SessionID = string.Empty;
        public string StartTime = string.Empty;
        public string CallType = string.Empty;

    }
}
