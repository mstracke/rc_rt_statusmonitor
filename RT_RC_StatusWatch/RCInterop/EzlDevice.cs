﻿using RingCentral;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RT_RC_StatusWatch.RCInterop
{
    public class EzlDevice
    {
        const string ENABLED = "Enabled";
        const string USER = "User";
        const string QUEUE = "Department";

        public string Name;
        public string Extention;
        private string eid;
        private StatusInfo statusInfo;
        private string extType;
        private List<DepartmentInfo> Departments;
        public string Status;
        public string StatusComment;
        public string StatusReason;

        public bool IsUser()
        {
            return extType.Contains(USER);

        }

        internal DepartmentInfo[] GetDepartmentInfo()
        {
            return Departments.ToArray();
        }

        public bool IsCallQueue()
        {
            return extType.Contains(QUEUE);
        }

        public string GetDeviceId()
        {
            return eid;
        }

        public bool IsEnabled()
        {
            return Status.Contains(ENABLED);
        }

        public EzlDevice(ExtensionInfo ext)
        {
            Departments = new List<DepartmentInfo>();
            this.Extention = ext.extensionNumber;
            this.eid = ext.id;
            this.Name = ext.name;
            this.Status = ext.status;
            this.statusInfo = ext.statusInfo;
            this.extType = ext.type;

        }


    }
}
