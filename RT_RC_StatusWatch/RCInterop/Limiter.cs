﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RT_RC_StatusWatch.RCInterop
{
    public class Limiter
    {
        public int CurrentAllowed = 0;

        public LimitType LimitTypeName { get; private set; }

        public System.Timers.Timer timer;
        public int LimitTick = 0;

        public static readonly object LimitLock = new object();

        public Limiter(int count, int maxSeconds, LimitType lt)
        {
            this.LimitTypeName = lt;
            timer = new System.Timers.Timer(1000);
            timer.Elapsed += Timer_Elapsed;
            this.MaxTime = maxSeconds;
            this.MaxCount = count;
            CurrentAllowed = MaxCount;
            timer.Start();
        }

        

        internal void Set(int amt = 1)
        {
            lock (LimitLock)
            {
                CurrentAllowed -= amt;
                //Program.Print(string.Format("CurrentAllowed {0}", CurrentAllowed));
            }

        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            LimitTick++;
            
            if (LimitTick >= MaxTime)
            {
                lock (LimitLock)
                {
                    CurrentAllowed = MaxCount;
                    LimitTick = 0;
                }
            }
            
        }

        public bool IsAllowed()
        {
            lock (LimitLock)
            {
                return CurrentAllowed > 0;
            }
            
        }

        public int MaxTime { get; private set; }
        public int MaxCount { get; private set; }

        internal int GetWaitTime()
        {
            lock (LimitLock)
            {
                //return (MaxTime - LimitTick) * 1000;
                return 60 * 1000;
            }
            
        }
    }

    public enum LimitType
    {
        Auth,
        Light,
        Medium,
        Heavy
    }
}
