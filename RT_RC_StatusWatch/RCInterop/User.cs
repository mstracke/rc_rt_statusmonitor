﻿using RT_RC_StatusWatch.RingCentralStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RingCentral;

namespace RT_RC_StatusWatch.RCInterop
{
    public class User
    {

        const string AVAILABLE = "Available";
        const string BUSY = "Busy";
        const string AFK = "AFK/DND";
        const string NODATA = "None";
        /*TABLE
         * id
         * Name
         * Ext
         * UID
         * PhoneStatus
         * DNDStatus
         * PresenceStatus
         * CombinedUserStatus
         * CurrentCallString
         * LastCallString
         */
        public string Name;
        public int Extention;
        public string UID;
        public EzlDevice Device;
        internal bool HasIncomingCall;
        internal TelephonyStatus UserTelephonyStatus;
        internal PresenceStatus UserPresenceStatus;
        internal DNDStatus UserDNDStatus;
        internal List<Call> ActiveCalls = new List<Call>();
        internal List<Call> CallLog = new List<Call>();
        internal CombinedStatus CurrentStatus = CombinedStatus.Available;

        internal TelephonyState LastTeleState = TelephonyState.None;
        internal PresenceState LastPresenceState = PresenceState.None;
        internal DNDState LastDNDState = DNDState.None;
        internal int LastActiveCallLogCount = 0;
        internal int LastCallLogCount = 0;

        private Dictionary<string, DetailedPresenceNotification> NotificationStore = new Dictionary<string, DetailedPresenceNotification>();
        public string TodaysHours = string.Empty;

        const string INBOUND = "Inbound";
        const string OUTBOUND = "Outbound";
        internal bool HasChange = false;
        public string IncomingCallData = NODATA;

        internal bool BelongsToMonitoredQueue = false;
        internal string UserName = string.Empty;
        internal bool RunQuickUpdate = false;
        internal int QuickPauseInterval = 500;
        internal TimeInterval[] UserHours;
        internal bool IsRefreshing = false;

        public string CurrentLastCallString { get; set; } = string.Empty;
        public string LastEventString { get; internal set; } = string.Empty;
        public string[] DisplayString { get; internal set; }

        public User(EzlDevice device)
        {
            this.Device = device;
            var _ext = 0;
            int.TryParse(device.Extention, out _ext);
            this.Extention = _ext;
            this.UID = device.GetDeviceId();
            this.Name = device.Name;


            this.UserDNDStatus = new DNDStatus();
            this.UserPresenceStatus = new PresenceStatus();
            this.UserTelephonyStatus = new TelephonyStatus();
            this.ActiveCalls = new List<Call>();
            this.CallLog = new List<Call>();



        }

        internal string CurrentCall()
        {
            if (ActiveCalls.Count > 0 && (UserTelephonyStatus.telephonyState == TelephonyState.CallConnected
                || UserTelephonyStatus.telephonyState == TelephonyState.OnHold
                || UserTelephonyStatus.telephonyState == TelephonyState.Ringing))
            {
                
                var acall = ActiveCalls.FirstOrDefault();
                if (acall.From == null)
                {
                    acall.From = new CallerInfo()
                    {
                        extensionNumber = "NA",
                        name = "NULL",
                        location = "NA",
                        phoneNumber = "NA"
                    };
                }
                    

                return string.Format("{0} {1}:{2} - {3}",
                    acall.Direction == null ? string.Empty : acall.Direction,
                    acall.Direction == null ? string.Empty : 
                    acall.Direction == INBOUND ? 
                    acall.From == null ? string.Empty : 
                    acall.From.phoneNumber : 
                    acall.To == null ? string.Empty : acall.To.phoneNumber == null ? string.Empty : acall.To.phoneNumber,
                    acall.Direction == null ? string.Empty : acall.Direction == INBOUND ? acall.From.name : acall.To.name,
                    acall.Duration == null ? -1 : acall.Duration);
            }
            else if (HasIncomingCall)
            {
                return IncomingCallData;
            }            
            else
            {
                return NODATA;
            }
        }

        

        internal string LastCall()
        {
            var hasACall = CurrentCall().Equals(NODATA);            

            /*if(ActiveCalls.Count == 0)
            {*/
                if (CallLog.Count > 0)
                {
                    
                    var acall = hasACall ? CallLog.Count >= 2 ? CallLog[1] : CallLog.First() : CallLog.First();
                    var time = new DateTime();
                    var startTime = acall.StartTime;
                    if (DateTime.TryParse(acall.StartTime, out time))
                        startTime = string.Format("{0:hh:mm:ss}", time);
                return string.Format("{0} {1}=>{2} at {3} for {4}",
                string.IsNullOrEmpty(acall.Direction) ? string.Empty : acall.Direction,
                acall.Direction == INBOUND || acall.Direction == null ? acall.From == null ? string.Empty : acall.From.phoneNumber : acall.To == null ? string.Empty : acall.To.phoneNumber,
                acall.Direction == INBOUND || acall.Direction == null ? acall.From == null ? string.Empty : acall.From.name : acall.To == null ? string.Empty : acall.To.name,
                    startTime,
                    acall.Duration == null ? 0 : acall.Duration);
                }
                else
                {
                    return NODATA;
                }
            /*} else
            {
                
                var acall = ActiveCalls.First();
                var time = new DateTime();
                var startTime = acall.StartTime;
                if (DateTime.TryParse(acall.StartTime, out time))
                    startTime = string.Format("{0:hh:mm:ss}", time);

                return string.Format("{0} {1}=>{2} at {3} for {4}",
                    acall.Direction,
                    acall.Direction == INBOUND ? acall.From.phoneNumber : acall.To.phoneNumber,
                    acall.Direction == INBOUND ? acall.From.name : acall.To.name,
                    startTime,
                    acall.Duration);


            }*/
            

        }

        

        internal bool StoreNotification(DetailedPresenceNotification dpNotification)
        {
            if (NotificationStore.Keys.Count == 0)
            {
                NotificationStore.Add(dpNotification.uuid, dpNotification);
                return true;
            }               
            else if (!NotificationStore.ContainsKey(dpNotification.uuid))
            {
                NotificationStore.Add(dpNotification.uuid, dpNotification);
                return true;
            }
            return false;  
        }

        public bool IsUser()
        {
            return this.Device.IsUser();
        }

        public bool IsCallQueue()
        {
            return this.Device.IsCallQueue();
        }

        public bool IsValid()
        {
            return (Extention != 0);
        }

        public string UserCombinedStatus()
        {
            if (UserTelephonyStatus.telephonyState == TelephonyState.CallConnected)
            {
                CurrentStatus = CombinedStatus.Busy;
                return BUSY + " On a Call";
            }

            if (UserTelephonyStatus.telephonyState == TelephonyState.Ringing)
            {
                CurrentStatus = CombinedStatus.Busy;
                return "Ringing";
            }


            if (UserDNDStatus.dndStatus == DNDState.TakeAllCalls
                && UserPresenceStatus.presenceState == PresenceState.Available
                && UserTelephonyStatus.telephonyState == TelephonyState.NoCall)
            {
                CurrentStatus = CombinedStatus.Available;
                return AVAILABLE;
            }

            if (UserDNDStatus.dndStatus == DNDState.DoNotAcceptAnyCalls
                && UserTelephonyStatus.telephonyState == TelephonyState.NoCall)
            {
                CurrentStatus = CombinedStatus.AFK;
                return AFK;
            }

            return AFK;
        }

        internal bool HasOpenPhone()
        {
            if (UserHours == null || UserHours.Length == 0)
                return false;

            var from = UserHours.FirstOrDefault().from;
            var to = UserHours.FirstOrDefault().to;
            DateTime dtFrom, dtTo;
            if (DateTime.TryParse(from, out dtFrom) &&
                DateTime.TryParse(to, out dtTo))
            {
                return DateTime.Now.CompareTo(dtFrom) >= 0 &&
                        DateTime.Now.CompareTo(dtTo) <= 0;
            }

            return false;
        }
    }

    public enum CombinedStatus
    {
        Available,
        Busy,
        AFK
    }
}
