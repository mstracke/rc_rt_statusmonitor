﻿using RingCentral;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RT_RC_StatusWatch.Util;

namespace RT_RC_StatusWatch.RCInterop
{
    public class UserManager
    {

        public static readonly object userLock = new object();

        private Dictionary<int, string> UserNameStore = new Dictionary<int, string>();

        private Dictionary<int, User> UserStore;

        private static UserManager _this = null;
        public static UserManager Instance
        {
            get
            {
                lock (userLock)
                {
                    if (_this == null)
                        _this = new UserManager();

                    return _this;
                }
            }
        }

        public Dictionary<int, User> GetUserStore()
        {
            return UserStore;
        }


        public string[] GetUserDeviceIds()
        {
            var devIds = new string[UserStore.Count];
            var keys = UserStore.Keys.ToArray();
            for (var i = 0; i < keys.Length; i++)
            {
                User tuser;
                if (UserStore.TryGetValue(keys[i], out tuser))
                {
                    devIds[i] = tuser.UID;
                }
            }


            return devIds;
        }

        public bool GetUserByDeviceId(string devid, out User user)
        {
            lock (userLock)
            {
                var keys = UserStore.Keys;
                foreach (var key in keys)
                {
                    User tuser;
                    if (UserStore.TryGetValue(key, out tuser))
                    {
                        if (tuser.UID.Equals(devid))
                        {
                            user = tuser;
                            return true;
                        }

                    }
                }
                user = null;
                return false;
            }
        }

        internal List<User> GetUserStoreList()
        {
            var lst = new List<User>();
            foreach (var key in UserStore.Keys)
            {
                lst.Add(GetUserByExt(key));
            }

            return lst;
        }

        public User GetUserByExt(int ext)
        {
            User user;
            UserStore.TryGetValue(ext, out user);
            return user;
        }

        public UserManager()
        {
            UserStore = new Dictionary<int, User>();
        }

        internal void AddUsers(ExtensionInfo[] records)
        {
            if (records.Length > 0)
            {
                foreach (var ext in records)
                {
                    var dev = new EzlDevice(ext);
                    if (dev.IsEnabled() && (dev.IsUser() || dev.IsCallQueue()))
                    {
                        var user = new User(dev);
                        if (user.IsValid()
                            && !UserStore.ContainsKey(user.Extention))
                        {
                            UserStore.Add(user.Extention, user);
                        }
                    }
                }
            }
            else
            {
                throw new Exception("No Users found");
                //Add custom exceptions
            }
        }

        internal void UpdateUser(User user)
        {
            lock (userLock)
            {
                var ext = user.Extention;
                UserStore.Remove(ext);
                UserStore.Add(ext, user);
            }
               
        }

        internal void SetMonitoredUsers(ConfigFile configFile)
        {
            UserNameStore = new Dictionary<int, string>(configFile.ConfigUsers);
            var username = string.Empty;
            foreach(var ext in UserNameStore.Keys)
            {
                if(UserNameStore.TryGetValue(ext, out username))
                {
                    var user = UserManager.Instance.GetUserByExt(ext);
                    user.UserName = username;
                    user.BelongsToMonitoredQueue = true;
                    UserManager.Instance.UpdateUser(user);
                }
            }
        }
    }
}
