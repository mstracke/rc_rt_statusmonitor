﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RT_RC_StatusWatch.RingCentralStatus
{
    public class DNDStatus : IStatus
    {
        const string Take_AllCalls = "TakeAllCalls";
        const string DoNot_Accept_Any_Calls = "DoNotAcceptAnyCalls";
        const string DoNot_Accept_Department_Calls = "DoNotAcceptDepartmentCalls";
        const string Take_Department_Calls_Only = "TakeDepartmentCallsOnly";
        private DNDState _dndStatus = DNDState.TakeAllCalls;
        public DNDState dndStatus
        {
            get
            {
                return _dndStatus;
            }
            set
            {
                this._dndStatus = value;
            }
        }

        public void Set(string value)
        {
            if (value.Equals(Take_AllCalls))
                dndStatus = DNDState.TakeAllCalls;
            if (value.Equals(DoNot_Accept_Any_Calls))
                dndStatus = DNDState.DoNotAcceptAnyCalls;
            if (value.Equals(DoNot_Accept_Department_Calls))
                dndStatus = DNDState.DoNotAcceptDepartmentCalls;
            if (value.Equals(Take_Department_Calls_Only))
                dndStatus = DNDState.TakeDepartmentCallsOnly;
        }

        public string GetStatusString()
        {
            var state = dndStatus;
            switch (state)
            {
                case DNDState.TakeAllCalls:
                    return Take_AllCalls;
                case DNDState.DoNotAcceptAnyCalls:
                    return DoNot_Accept_Any_Calls;
                case DNDState.DoNotAcceptDepartmentCalls:
                    return DoNot_Accept_Department_Calls;
                case DNDState.TakeDepartmentCallsOnly:
                    return Take_Department_Calls_Only;
                default:
                    return Take_AllCalls;
            }
        }

    }


    public enum DNDState
    {
        None,
        TakeAllCalls,
        DoNotAcceptAnyCalls,
        DoNotAcceptDepartmentCalls,
        TakeDepartmentCallsOnly,

    }
}
