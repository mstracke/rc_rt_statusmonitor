﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RT_RC_StatusWatch.RingCentralStatus
{
    public interface IStatus
    {

        string GetStatusString();
        void Set(string state);
    }
}
