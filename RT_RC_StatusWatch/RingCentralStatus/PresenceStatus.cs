﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RT_RC_StatusWatch.RingCentralStatus
{
    public class PresenceStatus : IStatus
    {
        const string OFFLINE = "Offline";
        const string BUSY = "Busy";
        const string AVAILABLE = "Available";

        private PresenceState _presenceState = PresenceState.Offline;
        public PresenceState presenceState
        {
            get
            {
                return _presenceState;
            }

            set
            {
                this._presenceState = value;
            }
        }

        public void Set(string value)
        {
            if (value.Equals(OFFLINE))
                presenceState = PresenceState.Offline;
            if (value.Equals(BUSY))
                presenceState = PresenceState.Busy;
            if (value.Equals(AVAILABLE))
                presenceState = PresenceState.Available;
        }


        public string GetStatusString()
        {
            var state = presenceState;
            switch (state)
            {
                case PresenceState.Available:
                    return AVAILABLE;

                case PresenceState.Busy:
                    return BUSY;

                case PresenceState.Offline:
                    return OFFLINE;

                default:
                    return OFFLINE;
            }
        }
    }

    public enum PresenceState
    {
        None,
        Offline,
        Busy,
        Available
    }
}
