﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RT_RC_StatusWatch.RingCentralStatus
{
    public class TelephonyStatus : IStatus
    {
        const string NOCALL = "NoCall";
        const string CALLCONNECTED = "CallConnected";
        const string RINGING = "Ringing";
        const string ONHOLD = "OnHold";
        const string PARKEDCALL = "ParkedCall";

        private TelephonyState _telephonyState = TelephonyState.NoCall;
        public TelephonyState telephonyState
        {
            get
            {
                return _telephonyState;
            }

            set
            {
                this._telephonyState = value;
            }
        }

        public void Set(string val)
        {
            if (val.Equals(NOCALL))
                telephonyState = TelephonyState.NoCall;
            if (val.Equals(CALLCONNECTED))
                telephonyState = TelephonyState.CallConnected;
            if (val.Equals(RINGING))
                telephonyState = TelephonyState.Ringing;
            if (val.Equals(ONHOLD))
                telephonyState = TelephonyState.OnHold;
            if (val.Equals(PARKEDCALL))
                telephonyState = TelephonyState.ParkedCall;
        }


        public string GetStatusString()
        {
            var state = telephonyState;
            switch (state)
            {
                case TelephonyState.NoCall:
                    return NOCALL;
                case TelephonyState.CallConnected:
                    return CALLCONNECTED;
                case TelephonyState.Ringing:
                    return RINGING;
                case TelephonyState.OnHold:
                    return ONHOLD;
                case TelephonyState.ParkedCall:
                    return PARKEDCALL;
                default:
                    return NOCALL;
            }
        }

    }

    public enum TelephonyState
    {
        None,
        NoCall,
        CallConnected,
        Ringing,
        OnHold,
        ParkedCall
    }
}
