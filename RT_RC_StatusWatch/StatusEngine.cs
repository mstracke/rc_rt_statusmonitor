﻿//TODO
/*
 *Add business hours so it only runs during our business hours.
 * Sort queue by closest hour
 * Change color of those with OFF
 */



using RingCentral;
using RT_RC_StatusWatch.RCInterop;
using RT_RC_StatusWatch.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RT_RC_StatusWatch
{
    internal class StatusEngine : IDisposable
    {
        private RestClient rc;
        internal static readonly object driveshaft = new object();
        private static StatusEngine _this = null;
        public TokenInfo RcToken { get; private set; }
        public bool UseRegisSubDelay { get; private set; }

        public float totalRegTime { get; set; }


        private static volatile Limiter heavyLimiter;
        private static volatile Limiter lightLimiter;
        private static volatile Limiter mediumLimiter;
        private static volatile Limiter authLimiter;

        private int subCount = 0;
        private Thread qUpdateThread;

        public delegate void UserNotificationEventHandler(UserNotificationEventArgs args);
        public event UserNotificationEventHandler UserNotificationEvent;


        private DateTime OpenTime = new DateTime();
        private DateTime CloseTime = new DateTime();

        #region Subscriptions
        internal async Task<bool> RegisterSubs(string[] deviceExtIdList)
        {
            Program.IsSubscribing = true;
            if(deviceExtIdList.Length > 10)
            {
                UseRegisSubDelay = true;
                var totalTime = deviceExtIdList.Length * 6.5f;
                Program.Print(string.Format("Using Sub Delay, Subscriptions will take {0} seconds to complete", totalTime));
            }

            foreach (var id in deviceExtIdList)
            {
                User user;
                if (UserManager.Instance.GetUserByDeviceId(id, out user))
                {
                    if (!user.BelongsToMonitoredQueue)
                        continue;

                    var subscription = rc.Restapi().Subscription().New();
                    subscription.EventFilters.Add(string.Format("/restapi/v1.0/account/~/extension/{0}/message-store", id));
                    subscription.EventFilters.Add(string.Format("/restapi/v1.0/account/~/extension/{0}/presence?detailedTelephonyState=true", id));
                    subscription.NotificationEvent += Subscription_NotificationEvent;

                    if (UseRegisSubDelay)
                        await Pause(6500);
                    Program.Print(string.Format("Subscribing to {0} presence.", user.Extention));
                    while (!mediumLimiter.IsAllowed())
                        await Pause(mediumLimiter.GetWaitTime());

                    if (mediumLimiter.IsAllowed())
                    {
                        mediumLimiter.Set(2);
                        await subscription.Register();
                        subCount++;
                    }
                }
            }
            Program.IsSubscribing = false;
            return subCount > 0;
        }

        private void Subscription_NotificationEvent(object sender, NotificatioEventArgs args)
        {

            if (Program.IsSubscribing)
                return;

            var notification = args.notification;
            var tstring = string.Empty;
            switch (notification.type)
            {
                case NotificationType.DetailedPresence:
                    var dpNotification = notification.Downcast<DetailedPresenceNotification>();

                    User user;
                    if (UserManager.Instance.GetUserByDeviceId(dpNotification.body.extensionId, out user))
                    {
                        OnUserNotification(dpNotification, user);
                    }
                    break;
                default:
                    break;

            }
        }



        private async void OnUserNotification(DetailedPresenceNotification dpNotification, User user)
        {
            Program.Print(string.Format("OnUserNotification {2} : {1} : {0}",
                dpNotification.body.dndStatus, dpNotification.body.telephonyStatus, user.Extention));
            var vuser = await RefreshSingleUser(user);
            if (vuser.StoreNotification(dpNotification))
            {
                if (!user.RunQuickUpdate)
                {
                    if (user.UserTelephonyStatus.telephonyState == RingCentralStatus.TelephonyState.Ringing ||
                        user.UserTelephonyStatus.telephonyState == RingCentralStatus.TelephonyState.CallConnected)
                        UserQuickUpdate(user);

                    //If user has a call ringing use quick update or if one is already connected and we missed the ring event.

                }
                vuser.LastEventString = string.Format("{0} - {1} - {2}",
               dpNotification.body.extensionId, dpNotification.timestamp, dpNotification.@event);
                Program.Print(vuser.LastEventString);
            }
           


           /* if (!user.RunQuickUpdate)
            {
                if (user.UserTelephonyStatus.telephonyState == RingCentralStatus.TelephonyState.Ringing ||
                    user.UserTelephonyStatus.telephonyState == RingCentralStatus.TelephonyState.CallConnected)                
                    UserQuickUpdate(user);                
                
                //If user has a call ringing use quick update or if one is already connected and we missed the ring event.
                
            }*/
                


            UserNotificationEvent?.Invoke(new UserNotificationEventArgs(vuser));

        }

        internal string GetAuthLmit()
        {
            return string.Format("{0}-{1}s", authLimiter.CurrentAllowed, authLimiter.LimitTick);           
        }

        internal string GetMediumLimit()
        {
            return string.Format("{0}-{1}s", mediumLimiter.CurrentAllowed, mediumLimiter.LimitTick);
        }

        internal string GetHeavyLimit()
        {
            return string.Format("{0}-{1}s", heavyLimiter.CurrentAllowed, heavyLimiter.LimitTick);
        }

        internal string GetLightLimit()
        {
            return string.Format("{0}-{1}s", lightLimiter.CurrentAllowed, lightLimiter.LimitTick);
        }

        private async Task<User> RefreshSingleUser(User user)
        {
            

           
            

            User vuser = null;

            await Pause(250);
            vuser = await RefreshUserLog(user, true);
            await Pause(1000);
            vuser = await RefreshUserStatus(user);
            await Pause(1000);
            //vuser = await RefreshUserHours(user);




            return vuser;
        }

        #endregion

        #region User Refresh

        private void SetHours(ref User user, TimeInterval[] range)
        {
            if (range != null)
            {
                if (range.Length == 0) return;
                user.TodaysHours = string.Format("{0} - {1}",
                    range.FirstOrDefault().from, range.FirstOrDefault().to);
                user.UserHours = range;
            }
            else
            {
                user.TodaysHours = "OFF";
                user.UserHours = new TimeInterval[0];
            }
        }

        internal void UserQuickUpdate(User user)
        {
            try
            {
                user.RunQuickUpdate = true;
                UserManager.Instance.UpdateUser(user);
                qUpdateThread = new Thread(new ParameterizedThreadStart(QuickUpdate));
                Program.ToDispose(qUpdateThread);
                qUpdateThread.Start(user.UID);
            }catch(Exception e)
            {
                Program.Print(string.Format("Failed to start QuickUpdate on {0} | Error: {1}", 
                    user.Extention, e.Message));
            }
                   
            
        }
        //Instead of passing User to QuickUpdate we pass string device id so I can call and modify the User in UserManager in that thread.
        //UserManager will handle the thread locking in Instance
        //Allowing me to stop quick update from any thread by modifying run bool.
        private void QuickUpdate(object obj)
        {
            
            var uid = obj as String;
            User user;
            if(UserManager.Instance.GetUserByDeviceId(uid, out user))
            {
                Program.Print(string.Format("Starting QuickUpdate on {0}", user.Extention));
                if (user != null && user.IsValid()
                && user.IsUser() && user.BelongsToMonitoredQueue)
                {
                    while (user.RunQuickUpdate)
                    {
                        QuickUpdateUser(user);
                        Thread.Sleep(user.QuickPauseInterval);
                    }
                }
                Program.Print(string.Format("Ending QuickUpdate on {0}", user.Extention));
            }
            
        }
        //make call to end run quick update before testing, otherwise endless loop
        private async void QuickUpdateUser(User user)
        {
            var _user = await QuickStatusRefresh(user);
            UserManager.Instance.UpdateUser(_user);
        }

        private async Task<User> QuickStatusRefresh(User user)
        {
            return await RefreshUserStatus(user);
        }

        

        internal async Task<User> RefreshUserHours(User user)
        {
            while (!lightLimiter.IsAllowed())
                await Pause(lightLimiter.GetWaitTime());
            lightLimiter.Set();
            if (lightLimiter.IsAllowed())
            {
                
                var _hours = await rc.Restapi().Account().Extension(user.UID).BusinessHours().Get();
                if (_hours != null)
                {
                    switch (DateTime.Now.DayOfWeek)
                    {
                        case DayOfWeek.Monday:
                            SetHours(ref user, _hours.schedule.weeklyRanges.monday);
                            break;
                        case DayOfWeek.Tuesday:
                            SetHours(ref user, _hours.schedule.weeklyRanges.tuesday);
                            break;
                        case DayOfWeek.Wednesday:
                            SetHours(ref user, _hours.schedule.weeklyRanges.wednesday);
                            break;
                        case DayOfWeek.Thursday:
                            SetHours(ref user, _hours.schedule.weeklyRanges.thursday);
                            break;
                        case DayOfWeek.Friday:
                            SetHours(ref user, _hours.schedule.weeklyRanges.friday);
                            break;
                        case DayOfWeek.Saturday:
                            SetHours(ref user, _hours.schedule.weeklyRanges.saturday);
                            break;
                        case DayOfWeek.Sunday:
                            SetHours(ref user, _hours.schedule.weeklyRanges.sunday);
                            break;
                    }
                }
            }
            
            return user;
        }


        private async Task<int> Pause(int len)
        {
            await Task.Delay(len);
            return len;
        }

        internal async Task<List<User>> RefreshUsers(string[] devIds)
        {
            
            foreach (var id in devIds)
            {
                User user;
                if (UserManager.Instance.GetUserByDeviceId(id, out user))
                {
                    if (!user.BelongsToMonitoredQueue)
                        continue;

                    Program.Print(string.Format("Refresh on {0}", user.Extention));
                    if (user.IsValid() && user.IsUser())
                    {
                        user = await RefreshUserHours(user);
                        await Pause(500);
                        if (user.HasOpenPhone())
                        {
                            user = await RefreshUserStatus(user);
                            await Pause(1000);

                            user = await RefreshUserLog(user);
                        }
                       
                        UserManager.Instance.UpdateUser(user);
                    }
                }
                await Pause(500);
            }

            return UserManager.Instance.GetUserStoreList();
        }

        public static readonly object LogLock = new object();

        private async Task<User> RefreshUserLog(User user, bool activeCallsOnly = false)
        {
            /*
            while (!heavyLimiter.IsAllowed())
                await Task.Delay(heavyLimiter.GetWaitTime());

            ActiveCallsPath.ListResponse activeCalls;
            heavyLimiter.Set();
            if (heavyLimiter.IsAllowed())
            {
               
                activeCalls = await rc.Restapi().Account().Extension(user.UID).ActiveCalls().List();
                await Task.Delay(500);
                if (activeCalls.records.Length > 0)
                {
                    var activeCallList = new List<Call>();
                    foreach (var item in activeCalls.records)
                    {
                        var call = new Call();
                        call.Action = item.action;
                        call.CallType = item.type;
                        call.Direction = item.direction;
                        call.Duration = item.duration;
                        call.From = item.from;
                        call.Legs = item.legs;
                        call.Result = item.result;
                        call.SessionID = item.sessionId;
                        call.StartTime = item.startTime;
                        call.To = item.to;
                        call.UID = item.id;

                        activeCallList.Add(call);
                    }
                    user.ActiveCalls = activeCallList;
                }
            }


           */

            if (!user.IsRefreshing)
            {
                user.IsRefreshing = true;
                while (!heavyLimiter.IsAllowed())
                    await Pause(heavyLimiter.GetWaitTime());

                heavyLimiter.Set();
                if (/*!activeCallsOnly &&*/ heavyLimiter.IsAllowed())
                {
                    await Pause(600);
                    var callLogData = await rc.Restapi().Account().Extension(user.UID).CallLog().List();

                    if (callLogData.records.Length > 0)
                    {
                        var callLogList = new List<Call>();
                        foreach (var item in callLogData.records)
                        {
                            var call = new Call();
                            call.Action = item.action;
                            call.CallType = item.type;
                            call.Direction = item.direction;
                            call.Duration = item.duration;
                            call.From = item.from;
                            call.Legs = item.legs;
                            call.Result = item.result;
                            call.SessionID = item.sessionId;
                            call.StartTime = item.startTime;
                            call.To = item.to;
                            call.UID = item.id;


                            callLogList.Add(call);
                        }

                        user.CallLog = callLogList;
                        //temp assign active calls to call log, may contain same data with one call.
                        user.ActiveCalls = callLogList;
                    }
                }
                user.CurrentLastCallString = GetLastCall(user);
                user.IsRefreshing = false;
                return user;

            }


            return user;

        }

        private async Task<User> RefreshUserStatus(User user)
        {
            while (!lightLimiter.IsAllowed())
                await Pause(lightLimiter.GetWaitTime());

            lightLimiter.Set();
            if (lightLimiter.IsAllowed())
            {
                try
                {
                    await Pause(250);
                    var presInfo = await rc.Restapi().Account().Extension(user.UID).Presence().Get();
                    user.UserTelephonyStatus.Set(presInfo.telephonyStatus);
                    user.UserDNDStatus.Set(presInfo.dndStatus);
                    user.UserPresenceStatus.Set(presInfo.presenceStatus);
                    if (user.RunQuickUpdate)
                    {
                        if (user.UserTelephonyStatus.telephonyState != RingCentralStatus.TelephonyState.CallConnected
                            && user.UserTelephonyStatus.telephonyState != RingCentralStatus.TelephonyState.Ringing)
                        {
                            user.RunQuickUpdate = false;
                            user.QuickPauseInterval = 1500;
                        }
                        else if (user.UserTelephonyStatus.telephonyState == RingCentralStatus.TelephonyState.CallConnected)
                        {
                            user.QuickPauseInterval = 3000;
                        }
                    }
                }catch(Exception e)
                {
                    Program.Print(e.Message);
                }
                
            }

           

            return user;
        }
        #endregion

        

        private string GetLastCall(User user)
        {
            return user.LastCall();
        }

        

        internal async Task<bool> Connect()
        {
            var isConected = false;
            while (!authLimiter.IsAllowed())
                await Pause(authLimiter.GetWaitTime());
            authLimiter.Set();
            if (authLimiter.IsAllowed())
            {
               
                
                if (await AuthRc())
                    isConected = await StatusEngine.Instance.GetExtentions();

               
            }

            return isConected;
        }



        private async Task<bool> GetExtentions()
        {

            while (!mediumLimiter.IsAllowed())
                await Pause(mediumLimiter.GetWaitTime());

            mediumLimiter.Set();
            if (mediumLimiter.IsAllowed())
            {
               
                var userRecs = await rc.Restapi().Account().Extension().List();
                UserManager.Instance.AddUsers(userRecs.records);
                return userRecs.records.Length > 0;
            }
            return false;
                
        }

        private async Task<bool> AuthRc()
        {
            rc = new RestClient(Program.settings.AppKey, Program.settings.AppSecret, Program.settings.IsProduction);
            var tokenInfo = await rc.Authorize(Program.settings.DevUser, Program.settings.AdminExt, Program.settings.Password);
            this.RcToken = tokenInfo;

            return RcToken.IsValid();
        }

        public void Dispose()
        {
            DisposeRC();            
        }

        private async void DisposeRC()
        {
            await rc.Revoke();
        }

        
        public static StatusEngine Instance
        {
            get
            {
                lock (driveshaft)
                {
                    if (_this == null)
                        _this = new StatusEngine();

                    return _this;
                }
            }
        }
        const int LimitInterval = 72;
        public StatusEngine()
        {
            heavyLimiter = new Limiter(10, LimitInterval, LimitType.Heavy);
            lightLimiter = new Limiter(50, LimitInterval, LimitType.Light);
            mediumLimiter = new Limiter(40, LimitInterval, LimitType.Medium);
            authLimiter = new Limiter(5, LimitInterval, LimitType.Auth);

            
        }

        private DateTime GetOpenTime()
        {
            var today = DateTime.Now;
            OpenTime = new DateTime(today.Year, today.Month, today.Day, today.DayOfWeek != DayOfWeek.Sunday ? 9 : 10, 0, 0);
            return OpenTime;

        }

        public bool IsQueueOpen(DateTime now)
        {
            return IsAfterOpen(now) && IsBeforeClose(now);
        }

        private DateTime GetCloseTime()
        {
            var today = DateTime.Now;
            CloseTime = new DateTime(today.Year, today.Month, today.Day, today.DayOfWeek != DayOfWeek.Sunday ? 23 : 21, 0, 0);
            return CloseTime;
        }

        private bool IsAfterOpen(DateTime now)
        {
            if (now.CompareTo(GetOpenTime()) >= 0)
                return true;
            else
                return false;
        }

        private bool IsBeforeClose(DateTime now)
        {
            if (now.CompareTo(GetCloseTime()) <= 0)
                return true;
            else
                return false;
        }


    }

    

    public class UserNotificationEventArgs : EventArgs
    {
        public bool Result = false;
        public User user;

        public UserNotificationEventArgs(User user, bool result = true)
        {
            this.user = user;
            this.Result = result;
        }
    }
}