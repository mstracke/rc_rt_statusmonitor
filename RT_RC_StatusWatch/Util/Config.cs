﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace RT_RC_StatusWatch.Util
{
    public class Config
    {
        const string configName = "SW_Config.cfg";
        
        public ConfigFile configFile
        {
            get
            {
                return ConfigFile.Instance;
            }
        }

        public Config()
        {
            lock (fLock)
            {
                if (!File.Exists(configName))
                    File.Create(configName);
                else
                    LoadConfigFile();
            }
        }

        private void LoadConfigFile()
        {
            lock (fLock)
            {
                var text = File.ReadAllText(configName);
                if (text.Length > 0)
                    ConfigFile.Instance = JsonConvert.DeserializeObject<ConfigFile>(text);

            }
        }

        public void SaveConfig(Dictionary<int, string> musers)
        {
            lock (fLock)
            {
                configFile.ConfigUsers = new Dictionary<int, string>(musers);
                var data = JsonConvert.SerializeObject(ConfigFile.Instance);
                File.WriteAllText(configName, data);
            }
            
        }
        public static readonly object fLock = new object();
        public void SaveConfig(string data)
        {
            lock (fLock)
            {
                File.WriteAllText(configName, data);
            }
        }

        public ConfigFile GetConfig()
        {
            return configFile;
        }

    }
}
