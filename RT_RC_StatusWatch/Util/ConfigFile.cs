﻿using System.Collections.Generic;

namespace RT_RC_StatusWatch.Util
{
    public class ConfigFile
    {
        public Dictionary<int, string> ConfigUsers;

        public static readonly object flock = new object();
        private static ConfigFile _this = null;
        public static ConfigFile Instance
        {
            get
            {
                lock (flock)
                {
                    if (_this == null)
                        _this = new ConfigFile();

                    return _this;
                }
            }
            set
            {
                lock (flock)
                {
                    _this = new ConfigFile(value);
                }
            }
        }

        public ConfigFile()
        {
            ConfigUsers = new Dictionary<int, string>();
        }

        public ConfigFile(ConfigFile cf)
        {
            this.ConfigUsers = new Dictionary<int, string>(cf.ConfigUsers);
        }

    }
}