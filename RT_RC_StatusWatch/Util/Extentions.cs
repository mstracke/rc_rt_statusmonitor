﻿using RingCentral;

namespace RT_RC_StatusWatch.Util
{
    public static class Extentions
    {

        public static bool IsValid(this TokenInfo tinfo)
        {
            return (null != tinfo ? string.IsNullOrEmpty(tinfo.access_token) ? false : tinfo.expires_in > 1 : false);                
        }

    }
}
