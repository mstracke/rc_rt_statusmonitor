﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RT_RC_StatusWatch.Util
{
    public class Settings
    {

        public Settings()
        {
            if (!ConfigurationManager.AppSettings.HasKeys())
                throw new ConfigurationErrorsException("Possibly Missing App.Config");
        }


        public string AppKey
        {
            get
            {
                return string.Format(ConfigurationManager.AppSettings["AppKey"]);
            }
        }

        public string AppSecret
        {
            get
            {
                return string.Format(ConfigurationManager.AppSettings["AppSecret"]);
            }
        }

        public string DevUser
        {
            get
            {
                return string.Format(ConfigurationManager.AppSettings["User"]);
            }
        }

        public string Password
        {
            get
            {
                return string.Format(ConfigurationManager.AppSettings["Password"]);
            }
        }

        public string AdminExt
        {
            get
            {
                return string.Format(ConfigurationManager.AppSettings["AdminExt"]);
            }
        }

        public bool IsProduction
        {
            get
            {
                var rt = 0;
                int.TryParse(string.Format(ConfigurationManager.AppSettings["IsProduction"]), out rt);
                return rt == 1;
            }
        }


    }
}
