﻿namespace RT_RC_StatusWatch
{
    partial class frmControlPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmControlPanel));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dgUsers = new System.Windows.Forms.DataGridView();
            this.cExt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCurCall = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cLastCall = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cDndStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPresence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTelephony = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hours = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastevent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblRCStatus = new System.Windows.Forms.Label();
            this.lblSystemStatus = new System.Windows.Forms.Label();
            this.txtUsers = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblLight = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lblMedium = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lblHeavy = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lblAuth = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.btnNewUsers = new System.Windows.Forms.Button();
            this.lblDbStatus = new System.Windows.Forms.Label();
            this.chkForceOpen = new System.Windows.Forms.CheckBox();
            this.lblSystemIndex = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUsers)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.dgUsers, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 83.99123F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.00877F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1016, 801);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // dgUsers
            // 
            this.dgUsers.AllowUserToOrderColumns = true;
            this.dgUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUsers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cExt,
            this.cName,
            this.cCurCall,
            this.cLastCall,
            this.cDndStatus,
            this.cPresence,
            this.cTelephony,
            this.hours,
            this.lastevent,
            this.username});
            this.tableLayoutPanel1.SetColumnSpan(this.dgUsers, 2);
            this.dgUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgUsers.Location = new System.Drawing.Point(3, 3);
            this.dgUsers.Name = "dgUsers";
            this.dgUsers.Size = new System.Drawing.Size(1010, 666);
            this.dgUsers.TabIndex = 1;
            // 
            // cExt
            // 
            this.cExt.HeaderText = "Ext.";
            this.cExt.Name = "cExt";
            this.cExt.ReadOnly = true;
            // 
            // cName
            // 
            this.cName.HeaderText = "Name";
            this.cName.Name = "cName";
            this.cName.ReadOnly = true;
            // 
            // cCurCall
            // 
            this.cCurCall.HeaderText = "Current Call";
            this.cCurCall.Name = "cCurCall";
            this.cCurCall.ReadOnly = true;
            // 
            // cLastCall
            // 
            this.cLastCall.HeaderText = "Last Call";
            this.cLastCall.Name = "cLastCall";
            this.cLastCall.ReadOnly = true;
            // 
            // cDndStatus
            // 
            this.cDndStatus.HeaderText = "DND Status";
            this.cDndStatus.Name = "cDndStatus";
            this.cDndStatus.ReadOnly = true;
            // 
            // cPresence
            // 
            this.cPresence.HeaderText = "Presence";
            this.cPresence.Name = "cPresence";
            this.cPresence.ReadOnly = true;
            // 
            // cTelephony
            // 
            this.cTelephony.HeaderText = "Phone Status";
            this.cTelephony.Name = "cTelephony";
            this.cTelephony.ReadOnly = true;
            // 
            // hours
            // 
            this.hours.HeaderText = "Phone Hours";
            this.hours.Name = "hours";
            // 
            // lastevent
            // 
            this.lastevent.HeaderText = "Last Event";
            this.lastevent.Name = "lastevent";
            // 
            // username
            // 
            this.username.HeaderText = "Username";
            this.username.Name = "username";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 10;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.Controls.Add(this.lblRCStatus, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblSystemStatus, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtUsers, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 7, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 8, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 9, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel6, 6, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtOutput, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnNewUsers, 9, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblDbStatus, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.chkForceOpen, 8, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblSystemIndex, 5, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 675);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1010, 123);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // lblRCStatus
            // 
            this.lblRCStatus.AutoSize = true;
            this.lblRCStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRCStatus.Location = new System.Drawing.Point(3, 0);
            this.lblRCStatus.Name = "lblRCStatus";
            this.lblRCStatus.Size = new System.Drawing.Size(95, 33);
            this.lblRCStatus.TabIndex = 0;
            this.lblRCStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSystemStatus
            // 
            this.lblSystemStatus.AutoSize = true;
            this.lblSystemStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSystemStatus.Location = new System.Drawing.Point(3, 33);
            this.lblSystemStatus.Name = "lblSystemStatus";
            this.lblSystemStatus.Size = new System.Drawing.Size(95, 33);
            this.lblSystemStatus.TabIndex = 1;
            this.lblSystemStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUsers
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.txtUsers, 3);
            this.txtUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtUsers.Location = new System.Drawing.Point(306, 3);
            this.txtUsers.Multiline = true;
            this.txtUsers.Name = "txtUsers";
            this.tableLayoutPanel2.SetRowSpan(this.txtUsers, 2);
            this.txtUsers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtUsers.Size = new System.Drawing.Size(297, 60);
            this.txtUsers.TabIndex = 3;
            this.txtUsers.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsers_KeyDown);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.lblLight, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(710, 36);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(95, 27);
            this.tableLayoutPanel3.TabIndex = 4;
            // 
            // lblLight
            // 
            this.lblLight.AutoSize = true;
            this.lblLight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLight.Location = new System.Drawing.Point(3, 13);
            this.lblLight.Name = "lblLight";
            this.lblLight.Size = new System.Drawing.Size(89, 14);
            this.lblLight.TabIndex = 1;
            this.lblLight.Text = "0";
            this.lblLight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Light Limit";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.lblMedium, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.Label3, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(811, 36);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(95, 27);
            this.tableLayoutPanel4.TabIndex = 5;
            // 
            // lblMedium
            // 
            this.lblMedium.AutoSize = true;
            this.lblMedium.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMedium.Location = new System.Drawing.Point(3, 13);
            this.lblMedium.Name = "lblMedium";
            this.lblMedium.Size = new System.Drawing.Size(89, 14);
            this.lblMedium.TabIndex = 2;
            this.lblMedium.Text = "0";
            this.lblMedium.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label3.Location = new System.Drawing.Point(3, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(89, 13);
            this.Label3.TabIndex = 1;
            this.Label3.Text = "Medium Limit";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.lblHeavy, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(912, 36);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(95, 27);
            this.tableLayoutPanel5.TabIndex = 6;
            // 
            // lblHeavy
            // 
            this.lblHeavy.AutoSize = true;
            this.lblHeavy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHeavy.Location = new System.Drawing.Point(3, 13);
            this.lblHeavy.Name = "lblHeavy";
            this.lblHeavy.Size = new System.Drawing.Size(89, 14);
            this.lblHeavy.TabIndex = 2;
            this.lblHeavy.Text = "0";
            this.lblHeavy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Heavy Limit";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.lblAuth, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(609, 36);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(95, 27);
            this.tableLayoutPanel6.TabIndex = 7;
            // 
            // lblAuth
            // 
            this.lblAuth.AutoSize = true;
            this.lblAuth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAuth.Location = new System.Drawing.Point(3, 13);
            this.lblAuth.Name = "lblAuth";
            this.lblAuth.Size = new System.Drawing.Size(89, 14);
            this.lblAuth.TabIndex = 1;
            this.lblAuth.Text = "0";
            this.lblAuth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Auth Limit";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtOutput
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.txtOutput, 5);
            this.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOutput.Location = new System.Drawing.Point(3, 69);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtOutput.Size = new System.Drawing.Size(499, 51);
            this.txtOutput.TabIndex = 8;
            // 
            // btnNewUsers
            // 
            this.btnNewUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNewUsers.Location = new System.Drawing.Point(912, 69);
            this.btnNewUsers.Name = "btnNewUsers";
            this.btnNewUsers.Size = new System.Drawing.Size(95, 51);
            this.btnNewUsers.TabIndex = 9;
            this.btnNewUsers.Text = "Add New Users";
            this.btnNewUsers.UseVisualStyleBackColor = true;
            this.btnNewUsers.Click += new System.EventHandler(this.btnNewUsers_Click);
            // 
            // lblDbStatus
            // 
            this.lblDbStatus.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.lblDbStatus, 2);
            this.lblDbStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDbStatus.Location = new System.Drawing.Point(104, 0);
            this.lblDbStatus.Name = "lblDbStatus";
            this.tableLayoutPanel2.SetRowSpan(this.lblDbStatus, 2);
            this.lblDbStatus.Size = new System.Drawing.Size(196, 66);
            this.lblDbStatus.TabIndex = 10;
            // 
            // chkForceOpen
            // 
            this.chkForceOpen.AutoSize = true;
            this.chkForceOpen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkForceOpen.Location = new System.Drawing.Point(811, 69);
            this.chkForceOpen.Name = "chkForceOpen";
            this.chkForceOpen.Size = new System.Drawing.Size(95, 51);
            this.chkForceOpen.TabIndex = 11;
            this.chkForceOpen.Text = "Force Open";
            this.chkForceOpen.UseVisualStyleBackColor = true;
            this.chkForceOpen.CheckedChanged += new System.EventHandler(this.chkForceOpen_CheckedChanged);
            // 
            // lblSystemIndex
            // 
            this.lblSystemIndex.AutoSize = true;
            this.lblSystemIndex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSystemIndex.Location = new System.Drawing.Point(508, 66);
            this.lblSystemIndex.Name = "lblSystemIndex";
            this.lblSystemIndex.Size = new System.Drawing.Size(95, 57);
            this.lblSystemIndex.TabIndex = 12;
            this.lblSystemIndex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 801);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmControlPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RC RT Status Monitor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmControlPanel_FormClosing);
            this.Load += new System.EventHandler(this.frmControlPanel_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgUsers)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dgUsers;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblRCStatus;
        private System.Windows.Forms.Label lblSystemStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn cExt;
        private System.Windows.Forms.DataGridViewTextBoxColumn cName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCurCall;
        private System.Windows.Forms.DataGridViewTextBoxColumn cLastCall;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDndStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPresence;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTelephony;
        private System.Windows.Forms.DataGridViewTextBoxColumn hours;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastevent;
        private System.Windows.Forms.DataGridViewTextBoxColumn username;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lblLight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label lblMedium;
        private System.Windows.Forms.Label Label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label lblHeavy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label lblAuth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Button btnNewUsers;
        private System.Windows.Forms.TextBox txtUsers;
        private System.Windows.Forms.Label lblDbStatus;
        private System.Windows.Forms.CheckBox chkForceOpen;
        private System.Windows.Forms.Label lblSystemIndex;
    }
}

