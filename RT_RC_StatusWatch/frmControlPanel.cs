﻿using RT_RC_StatusWatch.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Threading;
using RT_RC_StatusWatch.RCInterop;
using RT_RC_StatusWatch.Dashboard;
using Newtonsoft.Json;
using System.IO;

namespace RT_RC_StatusWatch
{
    public partial class frmControlPanel : Form
    {
        private static volatile Config config;
       
        private Thread SystemTick;
        private System.Timers.Timer GuiTick;
        public bool IsConnecting { get; private set; } = false;
        public bool SubscriptionsRegistered { get; private set; } = false;
        public bool IsRegisteringSubs { get; private set; } = false;
        public bool IsAwaitingUserRefresh { get; private set; } = false;
        public SystemStatus CurrentSysStatus { get; private set; } = SystemStatus.Idle;
        public bool AllowSubscriptions { get; private set; } = true;
        public bool IsLongDelayActive { get; private set; } = false;
        public string CurrentMonUsers { get; private set; }
        public bool RegisterSubscriptions { get; private set; } = false;

        public static volatile bool HasDisplayChange = false;

        public static volatile List<User> CurrentUserList = new List<User>();

        private static volatile int SystemIndex = -1;

        


        public frmControlPanel()
        {
            InitializeComponent();           
            
        }


        const string UserFile = "UserList.cfg";


        private void frmControlPanel_Load(object sender, EventArgs e)
        {
            config = new Config();
            

            if (config.GetConfig() == null || 
                config.GetConfig().ConfigUsers.Count == 0)
            {
                ConfigFile.Instance.ConfigUsers = new Dictionary<int, string>();                
                ConfigFile.Instance.ConfigUsers.Add(114, "adams");                
                ConfigFile.Instance.ConfigUsers.Add(117, "alex");               
                ConfigFile.Instance.ConfigUsers.Add(121, "bob");                
                ConfigFile.Instance.ConfigUsers.Add(106, "chad");
                ConfigFile.Instance.ConfigUsers.Add(105, "edwin");                
                ConfigFile.Instance.ConfigUsers.Add(111, "evan_ringelberg");
                ConfigFile.Instance.ConfigUsers.Add(113, "jeremyj");
                ConfigFile.Instance.ConfigUsers.Add(116, "josh_cullitan");
                ConfigFile.Instance.ConfigUsers.Add(109, "josh");
                ConfigFile.Instance.ConfigUsers.Add(122, "kris.deg");
                ConfigFile.Instance.ConfigUsers.Add(110, "mbrown");
                ConfigFile.Instance.ConfigUsers.Add(129, "rnicholas");
                ConfigFile.Instance.ConfigUsers.Add(118, "ssmith");
                ConfigFile.Instance.ConfigUsers.Add(107, "t.antal");
                ConfigFile.Instance.ConfigUsers.Add(119, "willyg");

                config.SaveConfig(ConfigFile.Instance.ConfigUsers);
            }
            
            SystemTick = new Thread(new ThreadStart(SystemTick_Elapsed));
            Program.ToDispose(SystemTick);
            SystemTick.Start();


            GuiTick = new System.Timers.Timer(250);
            GuiTick.Elapsed += GuiTick_Elapsed;
            Program.ToDispose(GuiTick);
            GuiTick.Start();

            StatusEngine.Instance.UserNotificationEvent += Instance_UserNotificationEvent;


            Program.SetVisible(btnNewUsers, !File.Exists(UserFile));
            


            Program.ToDispose(StatusEngine.Instance);
        }

        private void Instance_UserNotificationEvent(UserNotificationEventArgs args)
        {
            UserManager.Instance.UpdateUser(args.user);
            UpdateCurrentUserList(UserManager.Instance.GetUserStoreList());
            Program.Print(string.Format("Update Event for User: {0}", args.user.Extention));
        }

        private static volatile Dictionary<int, string[]> UserGUIStore = new Dictionary<int, string[]>();

        private void GuiTick_Elapsed(object sender, ElapsedEventArgs e)
        {

            Program.SetText(lblSystemIndex, SystemIndex.ToString());
            
            Program.SetText(lblRCStatus, string.Format("{0}", 
                Program.IsConnected ? "Connected" : "Not Connected"));
            Program.SetBackground(lblRCStatus,
                Program.IsConnected ? Color.Green : Color.Red);


            Program.SetText(lblSystemStatus, CurrentSysStatus.ToString());

            Program.SetText(lblAuth, StatusEngine.Instance.GetAuthLmit());
            Program.SetText(lblLight, StatusEngine.Instance.GetLightLimit());
            Program.SetText(lblMedium, StatusEngine.Instance.GetMediumLimit());
            Program.SetText(lblHeavy, StatusEngine.Instance.GetHeavyLimit());


            if (StatusEngine.Instance.IsQueueOpen(DateTime.Now))
            {
                if (config.GetConfig() != null || config.GetConfig().ConfigUsers.Count > 0)
                {
                    UserManager.Instance.SetMonitoredUsers(config.GetConfig());
                    var configdata = JsonConvert.SerializeObject(config.GetConfig());
                    if (CurrentMonUsers != configdata)
                    {
                        //UserManager.Instance.SetMonitoredUsers(config.GetConfig());
                        Program.SetText(txtUsers, configdata);
                        CurrentMonUsers = configdata;
                    }
                    
                }
                string[] ret;
                if (Program.GetUIText(txtOutput.Lines.Length, out ret))
                {
                    if (txtOutput.InvokeRequired)
                    {
                        txtOutput.BeginInvoke((MethodInvoker)delegate ()
                        {
                            txtOutput.Clear();
                            txtOutput.Lines = ret;
                        });
                    }
                    else
                    {
                        txtOutput.Clear();
                        txtOutput.Lines = ret;
                    }
                }

                if (UserGUIStore.Count > 0 && HasDisplayChange)
                {
                    HasDisplayChange = false;
                    if (dgUsers.InvokeRequired)
                    {
                        dgUsers.BeginInvoke((MethodInvoker)delegate ()
                        {
                            dgUsers.Rows.Clear();
                            dgUsers.Invalidate();
                        });
                    }
                    else
                    {
                        dgUsers.Rows.Clear();
                        dgUsers.Invalidate();
                    }

                    foreach (var key in UserGUIStore.Keys)
                    {
                        var rowItem = new string[8];
                        if (UserGUIStore.TryGetValue(key, out rowItem))
                        {
                            Program.Print(string.Format("Update: {0}", rowItem[0]));
                            if (dgUsers.InvokeRequired)
                            {
                                dgUsers.BeginInvoke((MethodInvoker)delegate ()
                                {
                                    dgUsers.Rows.Add(rowItem);
                                    dgUsers.Invalidate();
                                });
                            }
                            else
                            {
                                dgUsers.Rows.Add(rowItem);
                                dgUsers.Invalidate();
                            }
                            PushNotifications(key);
                            if (Program.LastResults.Count > 0)
                            {
                                var sb = new StringBuilder();
                                foreach (var result in Program.LastResults)
                                {
                                    sb.AppendFormat("{0}{1}", result, Environment.NewLine);
                                }
                                Program.SetText(lblDbStatus, sb.ToString());
                            }
                        }
                    }


                    Program.Print(string.Format("Updating user gui - {0} | {1}", CurrentUserList.Count, UserGUIStore.Count));

                }
            }
        }

        private void PushNotifications(int key)
        {
            DatabaseManager.Instance.UpdateStatus(UserManager.Instance.GetUserByExt(key));
        }

        private string[] GetRow(User user)
        {
            var row = new List<string>();
            row.Add(user.Extention.ToString());
            row.Add(user.Name);
            row.Add(user.CurrentCall());
            row.Add(user.CurrentLastCallString);
            row.Add(user.UserDNDStatus.GetStatusString());
            row.Add(user.UserPresenceStatus.GetStatusString());
            row.Add(user.UserTelephonyStatus.GetStatusString());
            row.Add(user.TodaysHours);            
            row.Add(user.LastEventString);
            row.Add(user.UserName);

            return row.ToArray();
        }
        
        private void SystemTick_Elapsed()
        {
            
            while (Program.IsAlive)
            {
                //if (!Program.IsConnected && !IsConnecting) 
                  //  DelayInc();

                if (StatusEngine.Instance.IsQueueOpen(DateTime.Now))
                {
                    OnSystemTick();
                    Thread.Sleep(250);
                }
                else if (chkForceOpen.Checked)
                {
                    OnSystemTick();
                    Thread.Sleep(250);
                }
                else
                {
                    if (Program.IsConnected)
                        StatusEngine.Instance.Dispose();
                    Program.IsConnected = false;
                    IsConnecting = false;
                    SystemIndex = 0;
                    //Program.Print("Queue Closed");
                    CurrentSysStatus = SystemStatus.Closed;
                    Task.Delay(60*1000).Wait();
                }
                    
                
            }
        }

        
        
        
        private async void ConnectRC()
        {
            Program.Print("Connecting to RC");
            Program.IsConnected = await StatusEngine.Instance.Connect();
            IsConnecting = false;
            DelayInc();
        }

        private async void Subscribe()
        {
            Program.Print("Subscribing");
            try
            {
                if (RegisterSubscriptions)
                    SubscriptionsRegistered = await StatusEngine.Instance.RegisterSubs(UserManager.Instance.GetUserDeviceIds());
                else
                    Program.Print("No Subscriptions");
            }
            catch(Exception e)
            {
                MessageBox.Show(string.Format("Error, Skipping subscribe | {0}", e.Message));
                Program.Print(string.Format("Subscriptions disabled | {0}", e.Message));
                AllowSubscriptions = false;
                SystemIndex = 1;
            }
            
            IsRegisteringSubs = false;
            DelayInc();
        }

        private void OnSystemTick()
        {
            //Program.Print(string.Format("Current SysIndex {0}", SystemIndex));
            switch (SystemIndex)
            {
                case 0:
                    CurrentSysStatus = SystemStatus.Connecting;
                    if (!IsConnecting && !Program.IsConnected)
                    {
                        IsConnecting = true;
                        ConnectRC();
                    }
                    
                    break;
                case 1:
                    CurrentSysStatus = SystemStatus.Active;
                    if (AllowSubscriptions)
                    {
                        if (!SubscriptionsRegistered && !IsRegisteringSubs)
                        {
                            IsRegisteringSubs = true;
                            Subscribe();
                        }
                    }
                    else
                    {
                        DelayInc();
                    }                                        
                    break;                    
                case 8:
                    CurrentSysStatus = SystemStatus.Active;
                    if (!IsAwaitingUserRefresh && !IsRegisteringSubs)
                    {
                        IsAwaitingUserRefresh = true;
                        RefreshUsers();
                    } 
                    break;
                case 9:
                    CurrentSysStatus = SystemStatus.Waiting;
                    Task.Delay(60 * 1000).Wait();
                    DelayInc(true);
                    /*if (!IsLongDelayActive)
                    {
                        IsLongDelayActive = true;
                       
                    }
                    else
                    {
                        Task.Delay(500).Wait();
                        DelayTick++;
                        if(DelayTick >= DelayInterval)  
                            DelayInc(true);
                    }*/
                    break;
                default:
                    CurrentSysStatus = SystemStatus.Idle;
                    DelayInc();
                    break;
            }

            
        }
        private static volatile int DelayTick = 0;
        private int DelayInterval = ((60 * 1000) * 2);

        private void DelayInc(bool IsReset = false)
        {
            Task.Delay(IsReset ? 5000 : 500).Wait();
            SystemIndex = IsReset ? 2 : SystemIndex+=1;
            if (IsLongDelayActive)
            {
                DelayTick = 0;
                IsLongDelayActive = false;
            }
                
        }

        private async void RefreshUsers()
        {
            Program.Print("Refresh Users");
            UserManager.Instance.SetMonitoredUsers(config.GetConfig());
            var _CurrentUserList = await StatusEngine.Instance.RefreshUsers(UserManager.Instance.GetUserDeviceIds());
            UpdateCurrentUserList(_CurrentUserList);
            
            DelayInc();
            IsAwaitingUserRefresh = false;
        }



        private void UpdateCurrentUserList(List<User> _CurrentUserList)
        {
            foreach (var user in _CurrentUserList)
            {

                if (user.IsValid() && user.IsUser())
                {
                    if (UserGUIStore.Count == 0)
                    {
                        Program.Print("HasChange True");
                        UserGUIStore.Add(user.Extention, GetRow(user));
                        HasDisplayChange = true;
                    }
                    else if (UserGUIStore.ContainsKey(user.Extention))
                    {
                        var _row = new string[8];
                        if (UserGUIStore.TryGetValue(user.Extention, out _row))
                        {
                            var crow = GetRow(user);

                            for (var i = 0; i < 8; i++)
                            {
                                if (!crow[i].Equals(_row[i]))
                                {
                                    Program.Print("HasChange True");
                                    HasDisplayChange = true;
                                    UserGUIStore.Remove(user.Extention);
                                    UserGUIStore.Add(user.Extention, crow);
                                    user.DisplayString = crow;
                                    UserManager.Instance.UpdateUser(user);
                                }
                            }
                        }
                    }
                    else if (!UserGUIStore.ContainsKey(user.Extention))
                    {
                        user.DisplayString = GetRow(user);
                        UserGUIStore.Add(user.Extention, user.DisplayString);
                        UserManager.Instance.UpdateUser(user);
                    }
                }
            }
            CurrentUserList = UserManager.Instance.GetUserStoreList();
        }

        private void frmControlPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.Shutdown();
        }

        private void txtUsers_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)            
                config.SaveConfig(JsonConvert.DeserializeObject<ConfigFile>(txtUsers.Text).ConfigUsers);//checks if json is correct
            
        }

        private void btnNewUsers_Click(object sender, EventArgs e)
        {
            var users = UserManager.Instance.GetUserStoreList();
            //var conv_users = JsonConvert.SerializeObject(users);
            //File.WriteAllText(UserFile, conv_users);
           
            if(users.Count > 0)
            {
                UserManager.Instance.SetMonitoredUsers(config.GetConfig());
                foreach (var user in UserManager.Instance.GetUserStoreList())
                {
                    DatabaseManager.Instance.InsertNewUser(user);
                }
            }
        }

        private void chkForceOpen_CheckedChanged(object sender, EventArgs e)
        {

        }
    }

    public enum SystemStatus
    {
        Connecting,
        Idle,
        Active,
        Waiting,
        Closed
    }

    
}
